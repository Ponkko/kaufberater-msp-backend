const db = require('../dbconnection');
const lodash = require('lodash');

const Product = {
  getAllProducts: function(callback) {
    return db.query('Select * from Products', callback);
  },

  getProjectById: function(id, callback) {
    return db.query('select * from Products where Id=?', [id], callback);
  },

  addProject: function(Product, callback) {
    return db.query(
      'Insert into Products values(Name,Short_Description,Description,Url,Technologies)',
      [
        Product.Name,
        Product.Short_Description,
        Product.Description,
        Product.Url,
        Product.Technologies,
      ],
      callback
    );
  },

  deleteProject: function(id, callback) {
    return db.query('delete from Products where Id=?', [id], callback);
  },

  updateProject: function(id, Product, callback) {
    return db.query(
      'update Products set Name=?,Short_Description=?,Description=?, Url=?, Technologies=? where Id=?',
      [
        Product.Name,
        Product.Short_Description,
        Product.Description,
        Product.Url,
        Product.Technologies, 
        id,
      ],
      callback
    );
  }
};

module.exports = Product;
